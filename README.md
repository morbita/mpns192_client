# mini-project

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.3.

## Instructions
1. Git clone https://bitbucket.org/morbita/mpns192_client/src/master/
2. Open the project
3. In the terminal of the project write: npm install
4. In the terminal of the project write: ng serve
5. Open the link to a client separated from the server: http://localhost:4200