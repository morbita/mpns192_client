import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.css']
})
//http request to download a file
export class DownloadComponent implements OnInit {
  SERVER_URL = "http://localhost:8080/mini_project/rest/file/download";
  downloadForm: FormGroup;  
  constructor(private formBuilder: FormBuilder, private httpClient: HttpClient) { }
  fileName;

  ngOnInit() {
    this.downloadForm = this.formBuilder.group({
      fileName:''
    });
  }
  //send the name of the file we want to download
  onFileSelect(event) {
    this.fileName = event.target.value;
    this.downloadForm.get('fileName').setValue(this.fileName); 
  }

  onSubmit() {
  }

}
