import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { UploadFileComponent } from './upload-file/upload-file.component';
import { DownloadComponent } from './download/download.component';
import {ReactiveFormsModule } from '@angular/forms';
import {ProgressBarModule} from "angular-progress-bar";
import { NgProgressModule } from 'ngx-progressbar';
import { MusicFilesComponent } from './music-files/music-files.component';

@NgModule({
  declarations: [
    AppComponent,
    UploadFileComponent,
    DownloadComponent,
    MusicFilesComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    ProgressBarModule,
    NgProgressModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


