import { Category } from './Category';

export class musicFile  {
    name: string;
    id: string;
    fileDuration: number;
    myCategory: Category;

}