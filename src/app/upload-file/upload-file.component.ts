import { Component, OnInit , Inject, Input} from '@angular/core';
import { musicFile } from '../musicFile';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { Category } from '../Category';
import { NgProgress } from 'ngx-progressbar';
import { map, catchError, switchMap } from 'rxjs/operators';
import { Observable, of, timer } from 'rxjs';



@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styles: []
})
export class UploadFileComponent implements OnInit {
  SERVER_URL = "http://localhost:8080/mini_project/rest/file/upload";
  uploadForm: FormGroup;  
  file;
  length;
  category;
  constructor(public ngProgress: NgProgress,private formBuilder: FormBuilder, private httpClient: HttpClient) { }


  ngOnInit() {
   
    this.uploadForm = this.formBuilder.group({
      profile: [''],
      length: 0,
      category: ''
    });
  }
  onFileSelect(event) {
    if (event.target.files.length > 0) {
       this.file = event.target.files[0];
      this.uploadForm.get('profile').setValue(this.file); 
    }
  }

  onLengthSelect(len){
   this.length = len.target.value;
    this.uploadForm.get('length').setValue(this.length); 
  }

  onCategorySelect(cat){
    this.category = cat.target.value;
    this.uploadForm.get('category').setValue(this.category); 
  }
//send the request and listen to the channel to get a response 
  onSubmit() {

    const formData = new FormData();
    formData.append('file', this.uploadForm.get('profile').value);
    formData.append('category', this.uploadForm.get('category').value);
    formData.append('length', this.uploadForm.get('length').value);
    this.ngProgress.start();
  
    this.httpClient.post<any>(this.SERVER_URL, formData).subscribe(
      (res) => {
        this.ngProgress.done();
        if(res==="all good"){
          alert("Your file has no virus and succesfully uploaded to the server");
      }
    else{
      alert("Your file has been rjected");
    }
    },
      (err) => {
        this.ngProgress.done();
        alert("You have an ERROR");
        console.log(err)
      }
    );
  }



}
