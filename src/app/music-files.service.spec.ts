import { TestBed } from '@angular/core/testing';

import { MusicFilesService } from './music-files.service';

describe('MusicFilesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MusicFilesService = TestBed.get(MusicFilesService);
    expect(service).toBeTruthy();
  });
});
