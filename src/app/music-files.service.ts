import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {Observable} from 'rxjs/Observable'

//operators
import "rxjs/add/operator/catch"
import 'rxjs/add/observable/throw';
import { throwError } from 'rxjs/internal/observable/throwError';
import { musicFile } from './musicFile';

@Injectable({
  providedIn: 'root'
})
export class MusicFilesService {

  constructor(private http:HttpClient) { }

  getFiles():Observable<musicFile[]>{
    return this.http.get<musicFile[]>("http://localhost:8080/mini_project/rest/file/getSongs")
    .catch(this.errorHandler)
  }

  errorHandler(error:HttpErrorResponse){
    return throwError(alert (JSON.stringify(error.error)||"Server Error"));
  }
 

}
