import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicFilesComponent } from './music-files.component';

describe('MusicFilesComponent', () => {
  let component: MusicFilesComponent;
  let fixture: ComponentFixture<MusicFilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusicFilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
