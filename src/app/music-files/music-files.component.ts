import { Component, OnInit } from '@angular/core';
import { MusicFilesService } from 'src/app/music-files.service';


@Component({
  selector: 'app-music-files',
  template: `
  <h2 style="color:lightblue;"> The music files:</h2>
  <style>
  table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
  }
  table {
  border-spacing: 15px;
  table-layout: fixed;
}
td {
  width: 25%;
  text-align: center;
}
th {
  text-align: center;
}
  </style>
<table style="width:100%">
  <tr>
    <th>Name</th>
    <th>Duration</th> 
    <th>Category</th>
  </tr>
  
    <tr *ngFor="let file of files">
      <td>{{file.name}}</td>
      <td>{{file.fileDuration}}</td>
      <td> {{file.myCategory}}</td>
    </tr>
 
</table>

  <br>
{{errorMsg}} 
  `,
  styles: []
})
export class MusicFilesComponent implements OnInit {
  public errorMsg="";
  public files=[];
  constructor(private _musicService:MusicFilesService) {  
   }
   //send the "get queue message" every 3 seconds, so the queue is updated.
  ngOnInit() {
    setInterval(() => {  this._musicService.getFiles ()
      .subscribe(data => this.files=data,
                error => {this.errorMsg=error
                  setTimeout(()=>{ 
                    this.errorMsg = "" }, 4000)
                }); }, 3000);
  }
  
}
